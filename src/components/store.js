import { createStore } from 'vuex'
import router from '../router'
import  {auth}  from '../firebase/firebaseInnit'
import {
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
  updateProfile,
  signOut
} from 'firebase/auth'

 /* eslint-disable */ 
export default createStore({
  state: {
    user: null
  },


  mutations: {
    SET_USER(state, user) {
      state.user = user;

    },
    CLEAR_USER(state, user) {
      state.user = null;
    }
  },
  actions: {
    async login({ commit }, details) {
      
      const { email, password, } = details;
      try {
        await signInWithEmailAndPassword(auth, email, password)
        
      } catch (error) {
        switch (error.code) {

          case 'auth/user-not-found':
            alert("User not found")
            break

            case 'auth/user-logged-in':
              alert("Logged in already")
              break

          case 'auth/wrong-password':
            alert("Wrong Password!")
            break

          default:
            alert("Check input fields!")
        }

        return
      }
      
      commit('SET_USER', auth.currentUser)

      router.push('/')

    },


    async register({ commit }, details) {
   

      const {username,institution,name,surname,email,password,password2} = details;
      
      if(password===password2){
        
      
      try {
        await createUserWithEmailAndPassword(auth, email, password)
        
      } catch (error) {
        
        switch (error.code) {

          case 'auth/email-already-in-use':
              alert("This Email is currently in use")

              case 'auth/email-already-exists':
                alert("This Email already exists, please try to log in")

              case 'auth/invalid-email': 
              alert("Invalid Email!")

              case 'auth/operation-not-allowed':
                alert("Operation not allowed")
                break
              case 'auth/weak-password':
                alert("You entered a weak password!")
                break
          default:
            alert("Take a look at the input fields again!")
        }


        
      }

     



      updateProfile(auth.currentUser, {
        displayName: username, photoURL: institution
      }).then(() => {
        console.log(auth.currentUser," UPDATE PROFILE");
      }).catch((error) => {
        
      });
      commit('SET_USER', auth.currentUser)

      	
      router.push('/')
    }
    else{
      alert("Password fields are not the same");
    }
    },


    async logout({ commit }) {

      await signOut(auth)
        commit ('CLEAR_USER') 
        router.push('/login')
      




    },


    fetchUser({commit}){
      
      auth.onAuthStateChanged(async user => { 
          console.log(auth.currentUser);
        if(user==null){
          commit('CLEAR_USER');
        }
        else {
          commit("SET_USER",user);
          if(router.isReady() && router.currentRoute.value.path=='/login'){
            router.push('/');
          }
        }


      })


    }

  }

})
